#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

from libneko.extras.superuser import SuperuserCog

admins = [377812572784820226]

class Superuser(SuperuserCog):
    async def owner_chk(self, ctx):
        return ctx.author.id in admins


def setup(bot):
    pass
    #bot.add_cog(Superuser())
