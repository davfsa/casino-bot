import urllib.parse
import re
from datetime import datetime

import discord
from discord.ext import commands
from discord.ext.commands import errors
from discord.ext.commands.converter import *

from utils import algorithms

class InspectionsCog(commands.Cog):
    @staticmethod
    async def is_banned_here(ctx, user_id):
        if not ctx.guild:
            return False

        try:
            bans = [ban.user for ban in await ctx.guild.bans()]
        except discord.Forbidden:
            return False
        else:
            return discord.utils.get(bans, id=user_id)


    @commands.command(
        name="snowflake", brief="Deciphers a snowflake."
    )
    async def inspect_snowflake_command(self, ctx, *, snowflakes: str):
            """You can pass up to 10 snowflakes at once."""
            if not snowflakes:
                return
            snowflake_re = re.compile(r"[^\d]*(\d+)[^\d]*")
            snowflakes = [int(snowflake) for snowflake in snowflake_re.findall(snowflakes)]

            if not snowflakes:
                return await ctx.send("No snowflakes found in that query. Sorry...")

            embed = discord.Embed(colour=algorithms.rand_colour())

            # Discord epoch from the Unix epoch in ms
            # Essentially the number of milliseconds since epoch
            # at 1st January 2015
            # epoch = 1_420_070_400_000
            for i, snowflake in enumerate(sorted(snowflakes[:10])):
                timestamp = (((snowflake >> 22) & 0x3FFFFFFFFFF) + 1_420_070_400_000) / 1000
                try:
                    creation_time = datetime.utcfromtimestamp(timestamp)
                except OverflowError:
                    return await ctx.send("Hey, that ain't a valid snowflake buddy.")

                worker_id = (snowflake & 0x3E0000) >> 17
                process_id = (snowflake & 0x1F000) >> 12
                increment = snowflake & 0xFFF

                desc = "\n".join(
                    (
                        f"`{creation_time} UTC`",
                        f"**Worker ID**: `{worker_id}`",
                        f"**Process ID**: `{process_id}`",
                        f"**Increment**: `{increment}`",
                    )
                )

                # Attempt to resolve the snowflake in all caches where possible
                # and non-intrusive to do so.

                if snowflake == getattr(ctx.bot, "client_id", None):
                    desc += "\n- My client ID"

                member = algorithms.find(lambda u: u.id == snowflake, ctx.guild.members)

                if snowflake == ctx.bot.owner_id:
                    desc += "\n- My owner's ID"
                    member = ctx.bot.get_user(ctx.bot.owner_id)

                if snowflake == ctx.bot.user.id:
                    desc += "\n- My user ID"
                    member = ctx.bot.user
                elif member:
                    desc += f"\n- Member in this guild ({member})"
                else:
                    try:
                        if algorithms.find(lambda u: u.id == snowflake, ctx.bot.users):
                            desc += f"\n- A member in another server I am in"
                        else:
                            desc += f"\n- A user I don't share a server with"

                        member = await ctx.bot.fetch_user(snowflake)

                        if hasattr(member, "nick"):
                            desc += f"\n- named {member}, nicked {member.nick}"
                        else:
                            desc += f"\n- named {member}"

                        if await self.is_banned_here(ctx, snowflake):
                            desc += "\n- \N{WARNING SIGN}\N{VARIATION SELECTOR-16}**USER IS BANNED HERE!**\N{WARNING SIGN}\N{VARIATION SELECTOR-16}"
                    except discord.NotFound:
                        desc += "\n- Doesn't point to anything valid, unfortunately"

                if not i and member:
                    embed.set_thumbnail(url=member.avatar_url)

                emoji = algorithms.find(lambda e: e.id == snowflake, ctx.guild.emojis)

                if emoji:
                    desc += f"\n- Emoji in this guild ({emoji})"
                elif algorithms.find(lambda e: e.id == snowflake, ctx.bot.emojis):
                    desc += "\n- Emoji in another guild"

                if algorithms.find(lambda c: c.id == snowflake, ctx.guild.categories):
                    desc += "\n- Category in this guild"

                if algorithms.find(lambda r: r.id == snowflake, ctx.guild.roles):
                    desc += "\n- Role in this guild"

                channel = ctx.bot.get_channel(snowflake)

                if channel:
                    if snowflake == ctx.channel.id:
                        desc += f"\n- ID for this channel ({channel})"
                    elif algorithms.find(lambda c: c.id == snowflake, ctx.guild.text_channels):
                        desc += f"\n- Text channel in this guild ({channel})"
                    elif algorithms.find(lambda c: c.id == snowflake, ctx.guild.voice_channels):
                        desc += f"\n- Voice channel in this guild ({channel})"
                    elif algorithms.find(lambda c: c.id == snowflake, ctx.bot.get_all_channels()):
                        desc += "\n- Text or voice channel in another guild"

                if snowflake == ctx.guild.id:
                    desc += "\n- ID for this guild"
                elif algorithms.find(lambda g: g.id == snowflake, ctx.bot.guilds):
                    desc += "\n- ID for another guild I am in"

                embed.add_field(name=snowflake, value=desc)

            await ctx.send("Snowflake inspection", embed=embed)

def setup(bot):
    bot.add_cog(InspectionsCog())
