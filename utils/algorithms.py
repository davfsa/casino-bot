"""
Algorithm stuff, and general bits and pieces that don't belong elsewhere, or
that generally operate on a wide variety of data.
"""
import contextlib
import time  # Basic timestamps


def find(predicate, iterable, default=None):
    """
    Attempts to find the first match for the given predicate in the iterable.

    If the element is not found, then ``None`` is returned.
    """
    for el in iterable:
        if predicate(el):
            return el

    return default


class TimeIt(contextlib.AbstractContextManager):
    def __init__(self):
        self.start = float("nan")
        self.end = float("nan")

    def __enter__(self):
        self.start = time.perf_counter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time.perf_counter()

    @property
    def time_taken(self):
        return self.end - self.start


def rand_colour() -> int:
    """Gets a random colour."""
    from random import randint

    return randint(0, 0xFFFFFF)
