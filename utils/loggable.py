import logging


__all__ = ("Loggable",)


class Loggable:
    __slots__ = ("logger",)

    def __init_subclass__(cls, **_):
        cls.logger = logging.getLogger(cls.__qualname__)
