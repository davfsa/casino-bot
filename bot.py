import discord
from discord.ext import commands

from asyncio import sleep
from os import listdir, getcwd
import traceback
import io
import os
from datetime import datetime
import logging

from libneko.pag import factory
from utils.loggable import Loggable

# MIT License
#
# Copyright (c) 2018 Davfsa
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

logging.basicConfig(level="INFO", format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

class Casino (commands.Bot, Loggable):
    def __init__(self):
        commands.Bot.__init__(self, command_prefix='c!')
        self.failed_cogs_on_startup = {}
        self.owner_id = 377812572784820226
        
    async def ext_load(self):
        # Imports modules
        path = getcwd() + '/ext/'
        cogs = []
        
        for f in listdir(path):
            if f.endswith('.py'):
                cogs.append('ext.' + f.replace('.py', ''))

        cogs.append('libneko.extras.superuser')
                
        for cog in cogs:
            try:
                self.load_extension(cog)
                self.logger.info(f"Loaded: {cog}")
            except BaseException as ex:
                self.logger.exception(f'Could not load {cog}', exc_info=(type(ex), ex, ex.__traceback__))
                self.failed_cogs_on_startup[cog] = ex
    
    async def on_ready(self):
        await self.ext_load()
        for cog, ex in self.failed_cogs_on_startup.items():
                    
            try:
                tb = ''.join(traceback.format_exception(type(ex), ex, ex.__traceback__))[-1500:]
                channel = self.get_channel(446291887524020224)
                await channel.send(f'FAILED TO LOAD {cog} BECAUSE OF {type(ex).__name__}: {ex}')
                    
                pag = factory.StringNavigatorFactory(max_lines=50, prefix='```py', suffix='```')
                for line in tb.split('\n'):
                    pag.add_line(line)
                        
                for page in pag.pages:
                    await channel.send(page)
            except Exception:
                self.logger.exception('Error occurred handling load error', exc_info=True)
        
        em = discord.Embed(title='Bot deployed', colour=discord.Colour.green(), timestamp=datetime.utcnow())
        msg = await bot.get_channel(446298417413750786).send(embed=em)
        await msg.add_reaction('✅')
        bot.remove_listener(self.on_ready)

    async def on_message(self, message):
        if message.author.bot is True:
            return
        else:
            await bot.process_commands(message)
        
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound) or isinstance(error, commands.CheckFailure):
            return
        if isinstance(error, commands.BadArgument):
            return await ctx.send("Invalid user")
    
        channel = bot.get_channel(446291887524020224)
        try:
            await ctx.send("Ups. An unexpected error has been raised, the error has been reported to the developer and will be "
                   "fixed soon :smile:")
        except:
            pass
        
        binder = factory.StringNavigatorFactory(max_lines=50, prefix='```py', suffix='```')
        
        error = error.__cause__ or error
        fmt = '**`Error in command {}`**\n\n**{}:**'.format(ctx.command, type(error).__name__)
        
        error_string = ''.join(traceback.format_exception(type(error), error, error.__traceback__))
        for line in error_string.split('\n'):
            binder.add_line(line)
        
        await channel.send(fmt)
        for i in binder.pages:
            await channel.send(i)
    
    async def on_guild_join(self, server):
        em = discord.Embed(title='Server joined', colour=discord.Colour.green(), timestamp=datetime.utcnow())
        em.set_thumbnail(url=server.icon_url)
        em.add_field(name='Server name:', value=server.name, inline=True)
        em.add_field(name='Server id:', value=server.id, inline=True)
        em.add_field(name='Server owner:', value=server.owner.mention, inline=True)
        em.add_field(name='Server owner id:', value=server.owner.id, inline=True)
        await bot.get_channel(446292018415665152).send(embed=em)
    

    async def on_guild_remove(self, server):
        em = discord.Embed(title='Server left', colour=discord.Colour.gold(), timestamp=datetime.utcnow())
        em.set_thumbnail(url=server.icon_url)
        em.add_field(name='Server name:', value=server.name, inline=True)
        em.add_field(name='Server id:', value=server.id, inline=True)
        em.add_field(name='Server owner:', value=server.owner.mention, inline=True)
        em.add_field(name='Server owner id:', value=server.owner.id, inline=True)
        await bot.get_channel(446292018415665152).send(embed=em)

bot = Casino()

async def presence():
    await bot.wait_until_ready()
    while not bot.is_closed():
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=f'{len(bot.users)} users | {len(bot.guilds)} servers'))
        await sleep(30)
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name='c!help'))
        await sleep(30)

bot.loop.create_task(presence())
with open("token.txt") as fp:
    token = fp.read()
bot.run(token)
